import { DataSource } from "typeorm";

import dotenv from "dotenv";
import { Message } from "./src/models/message.entity";

dotenv.config();

export const AppDataSource = new DataSource({
  type: "postgres",
  host: process.env.POSTGRES_HOST,
  port: 5432,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: true,
  logging: false,
  entities: [Message],
  subscribers: [],
  migrations: [],
});
