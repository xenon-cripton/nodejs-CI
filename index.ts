import express from "express";
import dotenv from "dotenv";
import "reflect-metadata";

import { AppDataSource } from "./data-source";

import messageRouter from "./src/controllers/message.controller";

AppDataSource.initialize()
  .then(() => {
    console.log("Database is connected");
  })
  .catch((err) => {
    console.error(err);
  });

dotenv.config();

const app = express();

const port = process.env.PORT;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/message", messageRouter);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
