import { Message } from "../models/message.entity";
import { AppDataSource } from "../../data-source";

export default AppDataSource.getRepository(Message);
