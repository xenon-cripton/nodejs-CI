import MessageDto from "../dto/message.dto";
import messageRepository from "../repository/message.repository";

const createMessage = async (message: MessageDto) => {
  try {
    if (!message.content) {
      throw new Error("Invalid message format. Content is required.").message;
    }

    const newMessage = messageRepository.create(message); 
    await messageRepository.save(newMessage); 

    return newMessage;
  } catch (e: any) {
    console.error("Error creating message:", e.message);
    return null;
  }
};

const getAllMessages = async () => {
  try {
    return await messageRepository.find();
  } catch (e: any) {
    console.error("Error getting messages:", e.message);
    return null;
  }
};

export { createMessage, getAllMessages };
